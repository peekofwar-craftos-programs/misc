--[[
    Program (c) Peekofwar
    August 2022

    v1.0
]]

local list = "/todo.txt"
local monitor = peripheral.wrap("bottom") or peripheral.find("monitor")
local function clearList()
    monitor.clear()
    monitor.setCursorPos(1,1)
end

local function splitter(inputstr, sep)
    local t={}
        for str in string.gmatch(inputstr, "([^"..(sep or " ").."]+)") do
            table.insert(t, str)
        end
    return t
end
        
        
local function renderList()
    clearList()
    monitor.setTextScale(0.5)
    local file = fs.open(list,"r")
    local contents = file.readAll()
    file.close()
    contents = splitter(contents,"\n")
    local w,h = monitor.getSize()
    monitor.setCursorPos(1,h/2-#contents/2)
    
    for i=1, #contents do
        local stub = string.sub(contents[i],1,3)
        local stub2 = string.sub(contents[i],1,6)
        if stub == "[x]" then
            monitor.setTextColor(colors.green)
        elseif stub2 == "[ ] * " then
            monitor.setTextColor(colors.yellow)
        elseif stub2 == "[ ] L " then
            monitor.setTextColor(colors.lightGray)
        elseif stub == "[-]" then
            monitor.setTextColor(colors.red)
        else
            monitor.setTextColor(colors.white)
        end
        monitor.write(contents[i])
        if i<#contents then 
            local x,y=monitor.getCursorPos()
            monitor.setCursorPos(1,y+1)
        end
    end
end

local function main()
    while true do
        renderList()
        term.clear()
        term.setCursorPos(1,1)
        print("Press ENTER to edit todo-list...")
        print("Press DELETE to exit.")
        local event, key = os.pullEvent("key")
        if key == keys.enter or key == keys.numPadEnter then
            clearList()
            monitor.write("Editing list...")
            shell.run("edit "..list)
            clearList()
            renderList()
        elseif key == keys.delete then
            clearList()
            monitor.write("Todo program closed.")
            return
        end
    end
end

main()

term.setCursorPos(1,1)
term.clear()
term.setTextColor(colors.yellow)
print(os.version())
