--[[
    Copyright (c) 2022 Peekofwar
]]

local step, slow
local args = {...}
for i=1, #args do
    if args[i] == "/step" then
        print("Step mode true")
        step = true
    end
    if args[i] == "/slow" then
        if args[i+1] and type(tonumber(args[i+1])) ~= "number" then
            error("Error in slow flag: Following value must be a number (in seconds)",0)
        end
        slow = tonumber(args[i+1]) or 0.05
    end
end

write("Enter a number: ")
x = read()

function isEven(x)
    return x/2 == math.floor(x/2)
end
int = 0
x_orig = x
x_highest = x
while true do
    if step then
        printError(x, isEven(x) and "EVEN" or "ODD")
        print(x/2, math.floor(x)/2)
    end
    term.setTextColor(colors.green)
    write(int.." ")
    if isEven(x) then
        term.setTextColor(colors.blue)
    else
        term.setTextColor(colors.yellow)
    end
    print(x)
    
    if x == 1 then break end
    
    if isEven(x) then
        x = x / 2
    else
        x = 3 * x + 1
    end
    int = int + 1
    x_highest = math.max(x_highest,x)
    --sleep(0)
    os.queueEvent("")
    os.pullEvent()
    if step then os.pullEvent("key") end
    if slow then sleep(slow) end
end
term.setTextColor(colors.yellow)
write("\n'")
term.setTextColor(colors.green)
write(x_orig)
term.setTextColor(colors.yellow)
write("' took ")
term.setTextColor(colors.lightBlue)
write(int)
term.setTextColor(colors.yellow)
write(" steps before reaching ")
term.setTextColor(colors.orange)
write(1)
term.setTextColor(colors.yellow)
write(" with a highest recorded value of '")
term.setTextColor(colors.green)
write(x_highest)
term.setTextColor(colors.yellow)
print("'.")
