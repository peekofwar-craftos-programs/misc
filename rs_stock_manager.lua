local item_list_path = "/.program_data/rs_stock_manager/stocklist.lua"

local rs_device = "rsBridge_0" -- todo: make this a config option

local version = "1.0.2"

local item_list = {}
local load_item_list; function load_item_list(forceedit,forcenew)
	if fs.exists(item_list_path) and not forcenew then
		local file = fs.open(item_list_path,"r")
		item_list = textutils.unserialise(file.readAll())
		file.close()
		if type(item_list) == "nil" then
			load_item_list(false,true)
		end
		--require("cc.pretty").pretty_print(item_list)
	else
		print("Generating new list...")
		sleep(2)
		local file = fs.open(item_list_path,"w")
		file.writeLine("--[[")
		file.writeLine("  RS Stock Manager Item List")
		file.writeLine("")
		file.writeLine("  format:")
		file.writeLine("  {name = \"mod_name:item_name\", quantity = number},")
		file.writeLine("  example:")
		file.writeLine("  {name = \"mekanism:enriched_redstone\", quantity = 16,},")
		file.writeLine("]]")
		file.writeLine("")
		file.writeLine("{")
		file.writeLine("  ")
		file.writeLine("}")
		file.close()
		shell.run("edit "..item_list_path)
		load_item_list()
	end
end
load_item_list()
local function main()
	local isError = false
	local rs = peripheral.wrap(rs_device)
	print("RS Stock Manager running...")
	print("Version: "..version)
	while true do
		if peripheral.isPresent(rs_device) then
			if isError then print("RS Bridge online!") isError = false end
			if #item_list < 1 then error("Item list is empty!",0) end
			for i=1, #item_list do
				if item_list[i] then
					local item = item_list[i].name
					local minimal = item_list[i].quantity
					local data = rs.getItem({name=item})
					local quantity = data and data.amount or 0
					local required = minimal - quantity
					--print(item,minimal,quantity)
					if not rs.isItemCrafting({name=item}) then
						if rs.isItemCraftable({name=item}) then
							if quantity < minimal then
								local result = rs.craftItem({name=item,count=required})
								if result then
									item_list[i].isError = false
									print("Queueing "..required.." "..item)
								elseif not item_list[i].isError then
									item_list[i].isError = true
									printError("Cannot queue "..required.." "..item)
								end
							elseif item_list[i].isError == true then
								item_list[i].isError = false
							end
						else
							printError("Pattern missing for "..item)
							table.remove(item_list,i)
						end
					end
				end
			end
		elseif not isError then
			isError = true
			printError("RS Bridge device is offline!")
		end
		sleep(2.5)
	end
end
main()
