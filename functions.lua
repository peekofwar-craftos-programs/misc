error([[
    This is just a bunch of random functions.
    This is not a program.
]],0)


local function password()
  local w,h = term.getSize()
  local x,y = term.getCursorPos()
  term.setCursorPos(x,w+2)
  local a = read("")
  term.setCursorPos(x,y)
  return a
end


local wrapper = require("cc.strings").wrap
function winPrint(win,...)
	local expect = require("cc.expect").expect
	expect(1,win,"table")
	local tx,ty = term.getCursorPos()
	local w,h = win.getSize()
	local input = {...}
	local str = ""
	for i=1,#input do
		str = str..tostring(input[i])
		if i<#input then str = str.." " end
	end
	local text = wrapper(str,w)
	for i=1,#text do
		local x,y = win.getCursorPos()
		win.write(text[i])
		if y==h then win.scroll(1) win.setCursorPos(1,y) else win.setCursorPos(1,y+1) end
	end
	term.setCursorPos(tx,ty)
end



local function colorPrint(scolor,...)
	local expect = require("cc.expect").expect
	local window = windows.log
	local isWrite = false
	local isWriteOverlay = false
	color = scolor
	if type(scolor) == "table" then
		expect(1,scolor[1],"table")
		expect(2,scolor[2],"number")
		
		window = scolor[1]
		color = scolor[2]
		if scolor[3] then
			expect(3,scolor[3],"boolean")
			isWrite = scolor[3]
			if scolor[4] then
				expect(4,scolor[4],"boolean")
				isWriteOverlay = scolor[4]
			end
		end
	end
	local current = window.getTextColor()
	window.setTextColor(color)
	if isWrite then
		if isWriteOverlay then
			local text = ...
			local current_background = window.getBackgroundColor()
			for i=1, #text do
				local x,y = window.getCursorPos()
				local w,h = window.getSize()
				if y>h then return error("Out of range!",2) end
				local line_text, f, b = window.getLine(y)
				local pos_color = colors.fromBlit(string.sub(b,x,x))
				window.setBackgroundColor(pos_color)
				local character = string.sub(text,i,i)
				window.write(character)
			end
			window.setBackgroundColor(current_background)
		else
			window.write(...)
		end
	else
		winPrint(window,...)
	end
	window.setTextColor(current)
	return
end