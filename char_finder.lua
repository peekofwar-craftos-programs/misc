--[[
	Char Finder (for lack of a better name)
	(c) 2023 Peekofwar
	
	v1.0 March 16th, 2023
	
	Displays all characters from CC's character
	set and displays the number associated with
	the selected character.
	
	Use Up/Down/Left/Right to navigate. 
	Press Enter or terminate to exit the program.
	
	Do whatever you wish with this program,
	just be sure to leave credit to the original
	author somewhere.
]]
local function writeInverted(...)
	local f,b=term.getTextColor(),term.getBackgroundColor()
	term.setTextColor(b) term.setBackgroundColor(f)
	write(...)
	term.setTextColor(f) term.setBackgroundColor(b)
	return
end
local function run()
	local s = 0
	local names = {"Null","White smiley face","Black smiley face","Black heart suit","Black diamond suit","Black club suit","Black spade suit","Bullet","Inverse bullet",nil,nil,"Male sign","Female sign",nil,"Eighth note","Beamed eighth notes","Black right-pointing pointer","Black left-pointing pointer","Up down arrow","Double exlamation mark","Pilcrow sign","Section sign","Black rectangle","Up down arrow with base","Upwards arrow","Downwards arrow","Rightwards arrow","Leftwards arrow","Right angle","Left right arrow","Black up-pointing triangle","Black down-pointing triangle","Space","Exclamation mark","Quotation mark","Number sign","Dollar sign","Percent sign","Ampersand","Apostrophe","Left parenthesis","Right parenthesis","Asterisk","Plus sign","Camma","Hyphen-minus","Full stop","Solidus","Digit zero","Digit one","Digit two","Digit three","Digit four","Digit five","Digit six","Digit seven","Digit eight","Digit nine","Colon","Semicolon","Less-than sign","Equals sign","Greather-than sign","Question mark","Commercial at","Latin capital letter a","Latin capital letter b","Latin capital letter c","Latin capital letter d","Latin capital letter e","Latin capital letter f","Latin capital letter g","Latin capital letter h","Latin capital letter i","Latin capital letter j","Latin capital letter k","Latin capital letter l","Latin capital letter m","Latin capital letter n","Latin capital letter o","Latin capital letter p","Latin capital letter q","Latin capital letter r","Latin capital letter s","Latin capital letter t","Latin capital letter u","Latin capital letter v","Latin capital letter w","Latin capital letter x","Latin capital letter y","Latin capital letter z","Left square bracket","Reverse solidus","Right square bracket","Circumflex accent","Low line","Grave accent","Latin small letter a","Latin small letter b","Latin small letter c","Latin small letter d","Latin small letter e","Latin small letter f","Latin small letter g","Latin small letter h","Latin small letter i","Latin small letter j","Latin small letter k","Latin small letter l","Latin small letter m","Latin small letter n","Latin small letter o","Latin small letter p","Latin small letter q","Latin small letter r","Latin small letter s","Latin small letter t","Latin small letter u","Latin small letter v","Latin small letter w","Latin small letter x","Latin small letter y","Latin small letter z","Left curly bracket","Vertical line","Right curly bracket","Tilde",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"No-break space","Inverted exclamation mark","Cent sign","Pound sign","Currency sign","Yen sign","Broken bar","Section sign","Diaeresis","Copyright sign","Feminine ordinal indicator","Left-pointing double angle quotation mark","Not sign","Soft hyphen","Registered sign","Macron","Degree sign","Plus-minus sign","Superscript two","Superscript three","Acute accent","Micro sign","Pilcrow sign","Middle dot","Cedilla","Superscript one","Masculine ordinal indicator","Right-pointing double angle quotation mark","Vulgar fraction one quarter","Vulgar fraction one half","Vulgar fraction three quarters","Inverse question mark","Latin capital letter a with grave","Latin capital letter a with acute","Latin capital letter a with circumflex","Latin capital letter a with tilde","Latin capital letter a with diaeresis","Latin capital letter a with ring above","Latin capital letter ae","Latin capital letter c with cedilla",}
	while true do
		term.clear() term.setCursorPos(1,1)
		local w,h = term.getSize()
		local c = math.floor(w/2)
		for i=0, 255 do
			local tx, ty = i+1, 1
			while tx > w do tx = tx - w ty = ty + 1 end
			term.setCursorPos(tx,ty)
			term.setTextColor(s==i and colors.white or colors.white)
			term.setBackgroundColor(s==i and colors.blue or colors.black)
			write(string.char(i))
		end
		term.setTextColor(colors.white)
		term.setBackgroundColor(colors.black)
		local x,y = term.getCursorPos()
		term.setCursorPos(c,y+2) print(string.char(31))
		term.setCursorPos(s<=9 and c-2 or s<=99 and c-3 or c-4,y+4) print(s)
		term.setCursorPos(c,y+4) write(string.char(30))
		write(" ") print(s<=31 and "Codepage 437" or s<=126 and "ASCII/ISO-8859-1" or s<=159 and "ComputerCraft" or "ISO-8859-1")
		if names[s+1] then term.setCursorPos(c-math.floor(#names[s+1]/2),y+5) print(names[s+1]) end
		for i=1,w do
			term.setCursorPos(i,y+3)
			local ch = s-c+i
			if ch >=0 and ch <= 255 then
				write(string.char(ch))
			end
		end
		term.setCursorPos(c-4,y+6) write(string.char(156)..string.char(140))
		writeInverted(string.char(147))
		term.setCursorPos(c-4,y+7) write(string.char(149).." ")
		writeInverted(string.char(149))
		term.setCursorPos(c-4,y+8) write(string.char(141)..string.char(140)..string.char(142))
		term.setCursorPos(c-3,y+7) write(string.char(s))
		
		term.setCursorPos(c+1,y+6) write(string.char(156)..string.char(140))
		writeInverted(string.char(147))
		term.setCursorPos(c+1,y+7) write(string.char(149).." ")
		writeInverted(string.char(149))
		term.setCursorPos(c+1,y+8) write(string.char(141)..string.char(140)..string.char(142))
		term.setCursorPos(c+2,y+7) writeInverted(string.char(s))
		
		
		
		local event,key = os.pullEvent("key")
		if key == keys.left then s = math.max(s-1,0)
		elseif key == keys.right then s = math.min(s+1,255)
		elseif key == keys.up then s = math.max(s-w,0)
		elseif key == keys.down then s = math.min(s+w,255)
		elseif key == keys.enter then term.clear() term.setCursorPos(1,1) return
		end
	end
end
run()
term.clear()