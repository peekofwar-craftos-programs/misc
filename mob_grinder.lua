--[[
    Program made by Peekofwar
    v1.0.0 - November 29, 2021
]]

shell.run("bg shell")
if multishell then
    multishell.setTitle(multishell.getCurrent(),"Bone Crusher")
end
local msg_attack = {
    [1] = "Have at thee!",
    [2] = "Die, you skeleton bone boy!",
    [3] = "Killing you is an act of mercy!",
    [4] = "'Allo Skeletor! Wait, you're not Skeletor!",
    [5] = "**Yawn...**",
    [6] = "Yummy items",
    [7] = "Target aquired!",
    [8] = "There you are!",
    [9] = "I see you!",
    [10] = "SURPRISE!!! **BANG**",
    [11] = "You brought a bow to a... mob farm. What'd you think was gonna happen?",
    [12] = "Stop resisting! You can't even hit me!",
    [13] = "You can't kill me, even if you aimed at me!",
    [14] = "Fus-ro-daAAAAAAAAA!",
    [15] = "You know as well as I, this ain't gonna end well for one of us.",
    [16] = "We both know this is one-sided.",
}
local msg_miss = {
    [1] = "Come out, come out wherever you are!",
    [2] = "Are you still there?",
    [3] = "Sleep mode activated.",
    [4] = "I'll wait.",
    [5] = "Come on, I don't have all day... Wait, actually I do.",
    [6] = "Do robots feel bordom?",
    [7] = "You can't hide forever!",
    [8] = "Come kiss me, I won't bite...",
    [9] = "Come on, I just want a hug!",
    [10] = "Hrmmmmmm....",
    [11] = "> minesweeper.exe",
    [12] = "Aren't you gonna come say 'hi'?",
    [13] = "Did I just miss?!",
    [14] = "Huh... I must've just missed him",
    [15] = "Can you spawn faster please?",
    [16] = "Hey, heeeey! Come down here!",
    [17] = "Why don't you come joing me in the sewer? I can make your items float! Wait, no I can't. This has to be the longest message... Look, I'm a bored little robot.",
}
print("Attacking... something..... probably")

local msg_wait = 0
local msg_select = "Hrm..."
local function msg(isMiss)
    if msg_wait > 0 then
        return msg_select
    elseif isMiss then
        if msg_wait > 0 then
            msg_wait = 0
            return msg_select
        else
            return msg_miss[math.random(1,#msg_miss)]
        end
    else
        msg_wait = 5
        return msg_attack[math.random(1,#msg_attack)]
    end
end
while true do
    term.clear()
    term.setCursorPos(1,1)
    local skeleton = turtle.attack()
    if msg_wait > 0 then
        msg_wait = msg_wait - 1
    end
    if not skeleton then
        msg_select = msg(true)
        print(msg_select)
        sleep(5)
    else
        msg_select = msg()
        print(msg_select)
    end
    sleep(0.5)
end
