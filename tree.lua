local args = {...}
--Program made by Peekofwar, v1.0.1 July 15, 2021
local indent = 1
local indentAmount = 2

local dirColor = 14
local fileColor = 1

local function getColor(color)
    return 2^(color-1)
end
local function printWait()
    term.setTextColor(getColor(5))
    write("Press any key to continue or F1 to cancel...")    local event, key, is_held = os.pullEvent('key')
    term.clearLine()
    if key == keys.f1 then
        local x,y = term.getCursorPos()
        term.setCursorPos(1,y)
        error()
    end
end
local tX,tY = term.getSize()
local function tree(path,addIndent)
    if addIndent then indent = indent + 1 end
    local directory = fs.list(path)
    for i = 1, #directory do
        local file = fs.combine(path, directory[i])
        local isDir = fs.isDir(file)
        local cx,cy = term.getCursorPos()
        if cy == tY then printWait() end
        term.setCursorPos(indent*indentAmount,cy)
        if isDir then
            term.setTextColor(getColor(dirColor))
        else
            term.setTextColor(getColor(fileColor))
        end
        print(directory[i])
        if isDir then
            tree(file,true)
        end
    end
    if addIndent then indent = indent - 1 end
end
local path = args[1] and shell.resolve(args[1])
local dirPath = path or shell.dir()
if not fs.exists(dirPath) then
    printError('The specified path could not be found.')
    error(nil,0)
end
tree(dirPath)
