--[[
	Network monitor
	(c) 2025 Peekofwar
	
	v1.0
]]

local monitor_id = "right"

local modem_id = "back"
local modem_channel = 39934

local enable_log = true
local log_file = "eventLog.txt"

local log = {
	enabled = false,
	path = "log.txt",
}
log.write = function(...)
	if log.enabled then
		local logfile = fs.open(log.path,"a")
		logfile.write(...)
		logfile.close()
	end
end
log.print = function(...)
	log.write(... .. "\n")
end

local function printCol(color,...)
	local text_color = term.getTextColor()
	term.setTextColor(color)
	print(...)
	term.setTextColor(text_color)
end
local function writeCol(color,...)
	local text_color = term.getTextColor()
	term.setTextColor(color)
	write(...)
	term.setTextColor(text_color)
end

local function run()
	term.clear()
	term.setCursorPos(1,1)
	printCol(colors.yellow,"Network Monitor")
	
	local monitor
	
	printCol(colors.lightGray,"Monitor configured as '"..monitor_id.."'")
	if peripheral.isPresent(monitor_id) then
		printCol(colors.green,"Monitor detected.")
		monitor = peripheral.wrap(monitor_id)
	else
		printError("Monitor disconnected.")
	end
	local current_terminal = term.current()
	
	monitor.setCursorPos(1,1)
	monitor.clear()
	monitor.setTextScale(0.5)
	
	local modem
	
	printCol(colors.lightGray,"Modem configured as '"..modem_id.."'")
	if peripheral.isPresent(modem_id) then
		printCol(colors.green,"Modem detected.")
		modem = peripheral.wrap(modem_id)
		printCol(colors.yellow,"Opening channel '"..modem_channel.."'...")
		modem.open(modem_channel)
	else
		printError("Modem disconnected.")
	end
	
	if enable_log then
		printCol(colors.green,"Logging enabled. Path: "..log_file)
		log.path = log_file
		log.enabled = true
	else
		printCol(colors.red,"Logging disabled.")
	end
	
	term.redirect(monitor)
	
	if modem.isOpen(modem_channel) then
		printCol(colors.green,"Listining on channel '"..modem_channel.."'...")
	else
		printError("Channel is not open.")
	end
	
-- ("!%H:%M:%S")
-- ("!%m/%d/%y")
	
	local pretty = require("cc.pretty")
	
	local event_blacklist = {
		["key"] = true,
		["key_up"] = true,
		["char"] = true,
		["mouse_click"] = true,
		["mouse_up"] = true,
		["mouse_drag"] = true,
		["mouse_scroll"] = true,
		["setting_changed"] = true,
	}
	
	log.print("\n######### LOG BEGIN ##########")
	log.print("    Log begins: "..os.date("%m/%d/%y @ %H:%M:%S"))
	
	while true do
		local event = {os.pullEvent()}
		local timestamp = os.date("%m/%d/%y @ %H:%M:%S")
		
		if event[1] == "key" and (event[2] == keys.enter or event[2] == keys.numPadEnter) then
			break
		elseif event[1] == "modem_message" then
			printCol(colors.green,timestamp.." - Modem Message")
			writeCol(colors.lightBlue,"Channel: "..event[3].." -> "..event[4])
			log.print("------------------------------")
			log.print(timestamp.." - Modem Message")
			log.write("Channel: "..event[3].." -> "..event[4])
			if event[6] then
				printCol(colors.blue," Dist: "..event[6].." metres")
				log.print(" Dist: "..event[6].." metres")
			else
				printCol(colors.yellow," Interdimentional")
				log.print(" Interdimentional")
			end
			pretty.pretty_print(event[5])
			log.print(textutils.serialize(event[5]))
		elseif event[1] == "chat" then
			writeCol(colors.lightGray,timestamp)
			writeCol(colors.lightBlue," <"..event[2].."> ")
			printCol(colors.white,event[3])
			log.print(timestamp.." @"..event[2]..": "..event[3])
		elseif not event_blacklist[event[1]] then
			printCol(colors.lightGray,timestamp.." - "..event[1])
			log.print("------------------------------")
			log.print(timestamp.." - "..event[1])
			for i=2, #event do
				printCol(colors.gray,tostring(event[i]))
				log.print(tostring(event[i]))
			end
		end
	end
	
	log.print("\n    Log ends: "..os.date("%m/%d/%y @ %H:%M:%S"))
	log.print("####### LOG TERMINATED #######")
	
	printError("Program ended.")
	term.redirect(current_terminal)
	printError("Program ended.")
	
		
end

run()