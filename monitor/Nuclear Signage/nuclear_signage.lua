args = {...}
local pos = {}
pos.x = args[1] or 1
pos.y = args[2] or 1

function printImage(path, display)
    local image = paintutils.loadImage(path)
    local terminal = term.current()
    term.redirect(display)
    paintutils.drawImage(image,1,1)
    term.redirect(terminal)
    return
end
function getColor(color)
    return 2^(color-1)
end
monitor = peripheral.wrap('top')
local x,y=monitor.getSize()
monitor.setTextScale(0.5)
printImage('/Pictures/nuclear_icon.nfp', monitor)
monitor.setBackgroundColor(getColor(2))
monitor.setTextColor(getColor(16))
monitor.setCursorPos(1,1)
monitor.write(' ! RADIATION ! ')
