--[[
	Power Meter
	(c) 2022 by Peekofwar
	v1.0 September 2, 2022
	
	Used to monitor energy flow.
]]

-- Configuration:
local display = peripheral.wrap("top")
local scale = false -- Set this to a number to force all graphs to a unified scale
local yield_time = 0.05 -- How long the program sleeps after drawing
local decimal_mode = "floor" -- Acceptable: floor, round, ceil, none
local meters = {
	{
		name = "energyDetector_6",
		label = "Basic",
		maxPower = 44*9,
		maxRecorded = 63.6,
	},
	{
		name = "energyDetector_7",
		label = "Adv",
		maxPower = 264,
		maxRecorded = 42.4,
	},
}

-- Start of program
local function convertToJoule(fe)
	local fe_conversion = 2.5
	local j = fe*fe_conversion
	local out = j
	local unit = "J/t"
	if out > 1000 then
		out = out/1000
		unit = "kJ/t"
	end
	if out > 1000 then
		out = out/1000
		unit = "MJ/t"
	end
	if out > 1000 then
		out = out/1000
		unit = "GJ/t"
	end
	if out > 1000 then
		out = out/1000
		unit = "TJ/t"
	end
	return out, unit, j
end
local function getMeterData(index)
	if not meters[index] then return end
	local name = meters[index].name
	local label = meters[index].label
	local range = meters[index].maxPower
	if peripheral.isPresent(name) then
		local flow = peripheral.call(name,"getTransferRate")
		local rate,unit,rawRate = convertToJoule(flow)
		meters[index].maxRecorded = math.max(meters[index].maxRecorded or 0,rawRate)
		return label,rate,range,unit,meters[index].maxRecorded or range,rawRate
	else
		return "OFFLINE",0,range,"",0
	end
end

local function barMeter(x, y, width, completed, items, text, text2, barColor, backgroundColor, terminal)
	function math.clamp(vMin,vMax,x)
		return math.max(math.min(x,vMax),vMin)
	end

	if not terminal then terminal = term.current() end
	--local percent = math.max(math.min(items / completed * 100,100),0)
	local percent = math.clamp(0,100,completed / items * 100)
	local oldTColor = terminal.getTextColor()
	local oldBColor = terminal.getBackgroundColor()
	local text2 = tostring(text2) or ""

    local current_terminal = term.current()
    term.redirect(terminal)
    
	term.setCursorPos(x, y-1) if text ~= nil then paintutils.drawLine(x,y-1,x+width-1,y-1) term.setCursorPos(x, y-1) write(text) term.setCursorPos(x+width-#text2,y-1) write(text2) end
	terminal.setBackgroundColor(backgroundColor or oldBColor)
	paintutils.drawLine(x,y,width+x-1,y)
	terminal.setBackgroundColor(barColor or oldTColor)
	if percent/100*width-1 > 0 --[[and width >= x]] then
		paintutils.drawLine(x,y,percent/100*width+x-1,y)
	end
    terminal.setBackgroundColor(oldBColor)
    terminal.setTextColor(oldTColor)
	term.redirect(current_terminal)
end

local function round(num)
	if decimal_mode == "none" then
		return num
	elseif decimal_mode == "round" then
		return math.floor(num+0.5)
	elseif decimal_mode == "floor" then
		return math.floor(num)
	elseif decimal_mode == "ceil" then
		return math.ceil(num)
	else
		error("'"..decimal_mode.."' is not a valid decimal mode!",0)
	end
end

if not display then error("Couldn't find configured display.",0) end
display.clear()
display.setCursorPos(1,1)
display.setTextScale(0.5)
local w,h = display.getSize()
print("Running power meter on display '"..peripheral.getName(display).."'")

while true do
	if peripheral.isPresent(peripheral.getName(display)) then
		display.setCursorPos(1,3)
		for i=1, #meters do
			local label,rate,range,unit,maximal,raw_rate = getMeterData(i)
			--print(label,rate,range,unit)
			if scale then maximal = scale end
			local x,y = display.getCursorPos()
			barMeter(2,y,w-2,raw_rate or 0,maximal or range or 1,label or "OFFLINE",(round(rate) or 0)..(unit or ""),colors.red,colors.gray,display)
			if i < #meters then 
				display.setCursorPos(x,y+3)
			end
		end
		sleep(yield_time)
	else
		printError("Display is OFFLINE!")
		sleep(5)
	end
end
