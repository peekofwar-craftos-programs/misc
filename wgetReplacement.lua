--[[
	Program made by Peekofwar
	(c) 2021-2022
	https://gitlab.com/Peekofwar
	v1.0.1 - Fixed not downloading to current dir
	
	It's a replacement for wget should you be
	running an ancient version of CraftOS which
	doesn't contain such a program.
	It does ask to overwrite an existing file,
	which the CraftOS wget can't do.
	
	wget https://gitlab.com/peekofwar-craftos-programs/misc/-/raw/main/wgetReplacement.lua
	pastebin get EYwhWkvd wgetReplacement.lua
]]
local function printUsage()
	print("Usage: \n"..shell.getRunningProgram().." <url> [filename]")
	print(shell.getRunningProgram().." run <url>")
end
local arguments = {...}
local function promptOverwrite()
	printError("File already exists.")
	print("Press [y] to overwrite, or press [n] to cancel.")
	term.setCursorBlink(true)
	while true do
		local event, key = os.pullEvent("char")
		if key == "y" then
			term.setCursorBlink(false)
			return true
		elseif key == "n" then
			printError("File download canceled.")
			term.setCursorBlink(false)
			return false
		end
	end
end
local function run()
	local request
	local destination
	local tempPath
	local isRun
	if arguments[1] == "run" then
		isRun = true
		request = arguments[2]
		tempPath = "/.temp/"..request:match("[^%/]*$")
	else
		request = arguments[1]
		destination = shell.resolve(arguments[2] or request:match("[^%/]*$"))
	end
	print("Fetching file from '"..request.."'...")
	local webfile,err = http.get(request)
	if err then
		printError("Failed with error: "..err)
	elseif isRun then
		local file = fs.open(tempPath,"w")
		file.write(webfile.readAll())
		file.close()
		print("Running '"..tempPath.."'...")
		shell.run(tempPath)
		fs.delete(tempPath)
	else
		if fs.exists(destination) and promptOverwrite() or not fs.exists(destination) then
			local file = fs.open(destination,"w")
			file.write(webfile.readAll())
			file.close()
			print("File saved as '"..destination.."'.")
		end
	end
end
if #arguments < 1 then
	printUsage()
end
if #arguments > 0 then
	run()
end
