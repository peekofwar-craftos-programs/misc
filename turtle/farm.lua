local progInfo = {
	path = shell.getRunningProgram(),
	extension = shell.getRunningProgram():match("[^%.]*$"),
	name = string.sub(shell.getRunningProgram(),1,#shell.getRunningProgram()-#shell.getRunningProgram():match("[^%.]*$")-1),
	appName = 'ACI Auto Farm',
	version = {
        string = '1.1.0',
	    date = 'June 20, 2021',
        build = 1,
    },
    devs = {"Peekofwar"},
	files = 
	{
		config = string.sub(shell.getRunningProgram(),1,#shell.getRunningProgram()-#shell.getRunningProgram():match("[^%.]*$")-1)..'.cfg',
		stats = string.sub(shell.getRunningProgram(),1,#shell.getRunningProgram()-#shell.getRunningProgram():match("[^%.]*$")-1)..'_stats.log',
	},
}
args = {...}
checkArgs = function()
	if args[1] == "/auto" then commands.autofarm("commandline") end
end

function start()
	while true do
		term.setCursorPos(1,1) term.clear()
		print(progInfo.appName)
		print("\nPlease enter command:\n")
		write("> ") input = read()
		
		if type(commands[input]) == "function" then
			commands[input]()
		else
			printError("Incomplete or malformed command.")
			pause()
		end
	end
end

config = {
	load = function()
		if fs.exists(progInfo.files.config) then
			local file = fs.open(progInfo.files.config,"r")
			config.current = textutils.unserialise(file.readAll())
			file.close()
		else
			print("Created "..progInfo.files.config)
			sleep(1)
			config.current = config.default
			config.save()
		end
	end,
	save = function()
		local file = fs.open(progInfo.files.config,"w")
		file.write(textutils.serialise(config.current))
		file.close()
	end,
	current = {},
	default = {
		program = {
			logRuns = false
		},
		calibration = {
			homeBlock = "mekanism:block_copper",
		},
		plot = {
			pos = {0,0},
			size = {10,10},
		},
		farm = {
			{
				seed = "minecraft:wheat_seeds",
				block = "minecraft:wheat",
				ripe_age = 7,
				plantable = true,
			},
		},
		fuel = {
			["minecraft:coal"] = true,
			["minecraft:charcoal"] = true,
			["minecraft:coal_block"] = true,
		},
		fertilizer = {
			maxFertilizer = 2,
			["minecraft:bone_meal"] = true,
		},
		autoFarmDelay = 52,
	},
}
routines = {
	farm = function()
		term.setCursorPos(1,1) term.clear()
			
		statistics.clear()
		
		if setups.calibrate() and setups.checkFuel(true) then
			term.setCursorPos(1,1) term.clear()
			
			if config.current.plot.pos[1] == 0 and config.current.plot.pos[2] == 0 then
				error("\nError: Plot position is not set.\n\nPlease edit plot information in the configuration file and run program again.\n",0)
			end
			
			print("Calibration: Passed\nFuel check: Passed")
			
			routines.fertilizer(true)
			
			print("\nStarting farming routine...")
			
			sleep(1)
			
			term.setCursorPos(1,1) term.clear()
			print("Farm pos: (".. config.current.plot.pos[1] ..",".. config.current.plot.pos[2] ..")")
			
			sleep(1)
			
			statistics.lastRun.startingFuel = turtle.getFuelLevel()
			
			turtle.up() statistics.lastRun.totalMoves = statistics.lastRun.totalMoves + 1
			
			repeat
				term.setCursorPos(1,1) term.clear()
				print("MOVING TO POS (".. config.current.plot.pos[1] ..",".. config.current.plot.pos[2] ..")")
				print("\nPos: (".. nav.pos[1] ..",".. nav.pos[2] ..")")
				nav.move("f",false,true)
			until nav.pos[2] == config.current.plot.pos[2]
			term.setCursorPos(1,1) term.clear()
			print("Pos: (".. nav.pos[1] ..",".. nav.pos[2] ..")")
			repeat
				term.setCursorPos(1,1) term.clear()
				print("MOVING TO POS (".. config.current.plot.pos[1] ..",".. config.current.plot.pos[2] ..")")
				print("\nPos: (".. nav.pos[1] ..",".. nav.pos[2] ..")")
				nav.move("l",false,true)
			until nav.pos[1] == config.current.plot.pos[1]
			term.setCursorPos(1,1) term.clear()
			print("Pos: (".. nav.pos[1] ..",".. nav.pos[2] ..")")
			
			local direction = true
			
			for i=1, config.current.plot.size[2] do
				for i=1, config.current.plot.size[1] do
					local fertalized = routines.fertilizer()
					--if fertalized then sleep(0.5) end
					local isRipe = routines.isRipe()
					
					if isRipe then
						turtle.digDown() statistics.lastRun.plantsFarmed = statistics.lastRun.plantsFarmed + 1
					end
					routines.plantSeeds(true)
					if i == config.current.plot.size[1] then break end
					if direction then
						nav.move("r",false,true)
					else
						nav.move("l",false,true)
					end
					term.setCursorPos(1,1) term.clear()
					print("Pos: (".. nav.pos[1] ..",".. nav.pos[2] ..")")
				end
				if i == config.current.plot.size[2] then break end
				direction = not direction
				nav.move("f",false,true)
				term.setCursorPos(1,1) term.clear()
				print("Pos: (".. nav.pos[1] ..",".. nav.pos[2] ..")")
			end
			
			repeat
				term.setCursorPos(1,1) term.clear()
				print("RETURNING HOME...")
				if direction then
					nav.move("l",false,true)
				else
					nav.move("r",false,true)
				end
				print("\nPos: (".. nav.pos[1] ..",".. nav.pos[2] ..")")
			until nav.pos[1] == 0
			term.setCursorPos(1,3) term.clear()
			repeat
				term.setCursorPos(1,1) term.clear()
				print("RETURNING HOME...")
				nav.move("b",false,true)
				print("\nPos: (".. nav.pos[1] ..",".. nav.pos[2] ..")")
			until nav.pos[2] == 0
			term.setCursorPos(1,1) term.clear()
			print("Pos: (".. nav.pos[1] ..",".. nav.pos[2] ..")")
			turtle.down() statistics.lastRun.totalMoves = statistics.lastRun.totalMoves + 1
			
			print("\nFarming routine finished!")
			sleep(1)
			nav.move("f",true)
			statistics.lastRun.resultingFuel = turtle.getFuelLevel()
			statistics.lastRun.hasRun = true
			statistics.logRun()
		else
			printError("\nAborting...")
			pause()
			return false
		end
	end,
	isRipe = function()
		local block, blockData = turtle.inspectDown()
		if block then
			for i=1, #config.current.farm do
				if blockData.name == config.current.farm[i].block and blockData.state.age == config.current.farm[i].ripe_age then
					return true
				else 
					return false
				end
			end
		end
	end,
	isEmptySoil = function()
		local block, blockData = turtle.inspectDown()
		if block then
			return false
		else 
			return true
		end
	end,
	plantSeeds = function(logStat)
		
		term.setCursorPos(1,3) term.clearLine() print("Checking for open plot...")
		if not routines.isEmptySoil() then return false end
		
		term.setCursorPos(1,3) term.clearLine() print("Looking for seeds...")
		local selectedSeed = false	--Check current slot first
		local slot = turtle.getItemDetail()
		if type(slot) == "table" then
			for i=1, #config.current.farm do
				if config.current.farm[i].seed and slot.name == config.current.farm[i].seed then
					selectedSeed = config.current.farm[i].seed
					break
				end
			end
		end
		if not selectedSeed then
			for i=1, 4*4 do
				if not routines.isEmptySoil() then break end
				turtle.select(i)
				slot = turtle.getItemDetail()
				if type(slot) == "table" then
					for i=1, #config.current.farm do
						if config.current.farm[i].seed and slot.name == config.current.farm[i].seed then
							selectedSeed = config.current.farm[i].seed
							break
						end
					end
				end
				if selectedSeed then break end
			end
		end
		
		if selectedSeed then
			term.setCursorPos(1,3) term.clearLine() print("Planting: "..selectedSeed)
			local OK = turtle.placeDown()
			if logStat and OK then
				statistics.lastRun.seedsPlanted = statistics.lastRun.seedsPlanted + 1
			end
		end
	end,
	fertilizer = function(check)
		if check then
		term.setCursorPos(1,1) term.clear()
			print("Checking for fertilizer...")
			hasFertilizer = 0
			
			for i=1, 4*4 do
				turtle.select(i)
				local slot = turtle.getItemDetail()
				if slot then 
					print(i, slot.name, slot.count)
				end
				if slot and config.current.fertilizer[slot.name] then
					hasFertilizer = hasFertilizer + slot.count
				end
			end
			return hasFertilizer > 0, hasFertilizer
		end
		
		if hasFertilizer == 0 or routines.isEmptySoil() or routines.isRipe() then return false end
		
		term.setCursorPos(1,3) term.clearLine()
		print("Fertilizing...")
		print("\nRemaining fertilizer: ".. hasFertilizer)
		
		local slot = turtle.getItemDetail()
		local counter = config.current.fertilizer.maxFertilizer
		
		if slot and config.current.fertilizer[slot.name] then
			for i=1, counter do
				if counter < 1 then break end
				local OK = turtle.placeDown()
				if OK then
					counter = counter - 1
					statistics.lastRun.fertilizerUsed = statistics.lastRun.fertilizerUsed + 1 or 1
				else
					break
				end
			end
		end
		
		for i=1, 4*4 do
			if counter < 1 then break end
			turtle.select(i)
			local slot = turtle.getItemDetail()
			
			if slot and config.current.fertilizer[slot.name] then
				for i=1, counter do
					if counter < 1 then break end
					local OK = turtle.placeDown()
					if OK then
						counter = counter - 1
						statistics.lastRun.fertilizerUsed = statistics.lastRun.fertilizerUsed + 1
					else
						break
					end
				end
			end
		end
		hasFertilizer = hasFertilizer - 1
		return true
	end,
	autofarm = function()
		local interval = config.current.autoFarmDelay or 52
		while true do
			local w,h = term.getSize()
			routines.farm()
			term.setCursorPos(1,1) term.clear()
			print("Auto Farm Routine")
			term.setCursorPos(1,h) term.write("Press ENTER to cancel")
			for i=interval*60, 0, -1 do
				term.setCursorPos(1,3) term.clearLine()
				print("Waiting " .. math.ceil(i/60).." minutes...")
				parallel.waitForAny(
					function() 
						return sleep(1)
					end, 
					function()
						while true do 
							local event, key os.pullEvent("key")
							if key == keys.enter or key == keys.numPadEnter then
								return true
							end
						end
					end
				)
			end
		end
	end,
}
statistics = {
	clear = function()
		statistics.lastRun = {
			hasRun = false,
			startingFuel = 0,
			predictedFuel = 0,
			resultingFuel = 0,
			plantsFarmed = 0,
			seedsPlanted = 0,
			totalMoves = 0,
			fertilizerUsed = 0,
		}
	end,
	logRun = function()
		if config.current.program.logRuns then
			local file = fs.open(progInfo.files.stats,"a")
			
			file.writeLine("### Run: "..os.date())
			file.writeLine("Version: "..progInfo.version.string.." build "..progInfo.version.build)
			file.writeLine("")
			file.writeLine("Moves: "..statistics.lastRun.totalMoves)
			file.writeLine("Starting fuel: "..statistics.lastRun.startingFuel)
			file.writeLine("Estimated fuel: "..statistics.lastRun.predictedFuel)
			file.writeLine("Actual used: "..statistics.lastRun.startingFuel-statistics.lastRun.resultingFuel)
			file.writeLine("")
			file.writeLine("Crops farmed: "..statistics.lastRun.plantsFarmed)
			file.writeLine("Seeds planted: "..statistics.lastRun.seedsPlanted)
			file.writeLine("Fertilizer used: "..statistics.lastRun.fertilizerUsed)
			file.writeLine("")
			file.writeLine("")
			file.close()
		end
	end,
}
nav = {
	dir = false,
	pos = false,
	move = function(direction, dontMove, logStat)
		if direction == "f" then
			if nav.dir == 1 then
				turtle.turnLeft()
			elseif nav.dir == 2 then
				turtle.turnLeft()
				turtle.turnLeft()
			elseif nav.dir == 3 then
				turtle.turnRight()
			end
			nav.dir = 0
			if not dontMove then local pass, fail = turtle.forward()
			if pass then nav.pos[2] = nav.pos[2] + 1 end end
		end
		if direction == "b" then
			if nav.dir == 3 then
				turtle.turnLeft()
			elseif nav.dir == 0 then
				turtle.turnLeft()
				turtle.turnLeft()
			elseif nav.dir == 1 then
				turtle.turnRight()
			end
			nav.dir = 2
			if not dontMove then local pass, fail = turtle.forward()
			if pass then nav.pos[2] = nav.pos[2] - 1 end end
		end
		if direction == "l" then
			if nav.dir == 0 then
				turtle.turnLeft()
			elseif nav.dir == 1 then
				turtle.turnLeft()
				turtle.turnLeft()
			elseif nav.dir == 2 then
				turtle.turnRight()
			end
			nav.dir = 3
			if not dontMove then local pass, fail = turtle.forward()
			if pass then nav.pos[1] = nav.pos[1] - 1 end end
		end
		if direction == "r" then
			if nav.dir == 0 then
				turtle.turnRight()
			elseif nav.dir == 2 then
				turtle.turnLeft()
			elseif nav.dir == 3 then
				turtle.turnLeft()
				turtle.turnLeft()
			end
			nav.dir = 1
			if not dontMove then
				local pass, fail = turtle.forward()
				if pass then
					nav.pos[1] = nav.pos[1] + 1
					if logStat then
						statistics.lastRun.totalMoves = statistics.lastRun.totalMoves + 1
					end
				end
			end
		end
	end,
	jog = function()
		term.setCursorPos(1,1) term.clear()
		print("JOG MODE")
		if nav.dir == false or nav.pos == false then
			printError("\WARN: Turtle not homed.")
			pause()
			nav.dir = nav.dir or 0
			nav.pos = nav.pos or {0,0}
		end
		term.setCursorPos(1,1) term.clear()
		print("JOG MODE")
		print("\nUse WASD keys to move\nPress ENTER to exit")
		
		while true do
			term.setCursorPos(1,7)
			term.clearLine() term.write("Pos: "..nav.dir.." ("..nav.pos[1]..","..nav.pos[2]..")")
			local event, key = os.pullEvent("key")
			if key == keys.enter or key == keys.numPadEnter then
				return
			elseif key == keys.w then
				nav.move("f")
			elseif key == keys.a then
				nav.move("l")
			elseif key == keys.d then
				nav.move("r")
			elseif key == keys.s then
				nav.move("b")
			elseif key == keys.leftShift then
				turtle.up()
			elseif key == keys.leftCtrl then
				turtle.down()
			end
		end
	end,
}
setups = {
	calibrate = function()
		term.setCursorPos(1,1) term.clear()
		local fault = false
		print("Calibrating...")
		sleep(0.5)
		write("\nPosition marker: ")
		local _, check = turtle.inspectDown()
		if check.name == config.current.calibration.homeBlock then
			print("OK")
			write("Orientation marker: ")
			for i=1, 5 do
				local _, checkFore = turtle.inspect()
				if i == 5 then
					printError("FAULT")
					fault = true
					break
				elseif checkFore.name == config.current.calibration.homeBlock then
					print("OK")
					break
				else
					turtle.turnLeft()
				end
			end
		else
			printError("FAULT")
			fault = true
		end
		if fault then 
			printError("\nCalibration failed!\n")
			pause()
			return false
		else
			print("\nCalibration sucsessful!")
			turtle.turnRight()
			turtle.turnRight()
			nav.dir = 0
			nav.pos = {0,0}
			print("\nReady\n")
			sleep(1)
			return true
		end
	end,
	checkFuel = function(logStat)
		term.setCursorPos(1,1) term.clear()
		print("Checking fuel status...\n")
		sleep(0.5)
		local fuel = turtle.getFuelLevel()
		
		
		-- Turtle first moves up:
		local fuelNeeded = 0
		-- moving from HOME:
		fuelNeeded = fuelNeeded + math.abs(config.current.plot.pos[1]) + math.abs(config.current.plot.pos[2])
		-- navigate plot:
		fuelNeeded = fuelNeeded + config.current.plot.size[1] * config.current.plot.size[2]
		-- return trip:
		local side = isEven(config.current.plot.size[1]*config.current.plot.size[2]) and "right" or "left"
		print("Resulting side: "..side)
		if side == "right" then
			fuelNeeded = fuelNeeded + config.current.plot.size[1]+config.current.plot.pos[1]-1 + config.current.plot.size[2]+config.current.plot.pos[2]-1
		else
			fuelNeeded = fuelNeeded + config.current.plot.pos[1] + config.current.plot.size[2]+config.current.plot.pos[2]-1
		end
		print("\nFuel: "..fuel)
		print("Required: "..fuelNeeded)
		
		if logStat then statistics.lastRun.predictedFuel = fuelNeeded end
		
		if fuelNeeded > fuel then
			printError("\nInsuficient fuel!")
			pause()
			return false
		else
			print("\nReady")
			sleep(1)
			return true
		end
	end,
	refuel = function()
		term.setCursorPos(1,1) term.clear()
		print("Enter ammount to refuel")
		write("> ") local amount = read()
		amount = runString(amount)
		if type(tonumber(amount)) == "number" then
			--turtle.select(1)
			for i=1, 4*4 do
				if turtle.getFuelLevel() == turtle.getFuelLimit() then
					printError("\nCan't add more fuel; at capacity")
					pause()
					amount = 0
					break
				end
				turtle.select(i)
				local slot = turtle.getItemDetail()
				if type(slot) == "table" then
					if config.current.fuel[slot.name] then
						for i=1, slot.count do
							if amount == 0 then break end
							turtle.refuel(1)
							amount = amount - 1
						end
					end
				end
				if amount == 0 then break end
			end
		else
			printError("\nInvalid; not a number.\n")
			pause()
		end
		if amount > 0 then
			printError("\nCould not find "..amount.." more fuel.")
			pause()
			return false
		end
	end,
}
isEven = function(a)
	if type(a) ~= "number" then return error("expected number, got '"..type(a).."' instead") end
	return math.floor(a/2) == a/2
end
pause = function()
	local w,h = term.getSize()
	local x,y = term.getCursorPos()
	term.setCursorPos(1,h)
	term.write("Press any key to continue...")
	local event = os.pullEvent("key")
	term.clearLine()
	term.setCursorPos(x,y)
end
function runString(s)
	if type(s) ~= "string" then return false end
	local a = assert(loadstring("return ".. s))
	local b = a(s)
	if type(b) == "number" then
		return b
	else
		return false
	end
end
commands = {
	farm = function()
		routines.farm()
	end,
	autofarm = function()
		routines.autofarm()
	end,
	refuel = function()
		setups.refuel()
	end,
	fuel = function()
		term.setCursorPos(1,1) term.clear()
		print("Fuel: "..turtle.getFuelLevel().."/"..turtle.getFuelLimit())
		pause()
	end,
	jog = function()
		nav.jog()
	end,
	calibrate = function()
		setups.calibrate()
	end,
	home = function()
		setups.calibrate()
	end,
	ver = function()
		term.setCursorPos(1,1) term.clear()
		print(progInfo.appName)
		print("\nv"..progInfo.version.string.." build "..progInfo.version.build)
		print(progInfo.version.date)
		pause()
	end,
	stats = function()
		term.setCursorPos(1,1) term.clear()
		if not statistics.lastRun then
			print("No data available for previous run.")
			sleep(1)
			pause()
			return false
		end
		print("Last run statistics:\n")
		print("Starting fuel: "..statistics.lastRun.startingFuel)
		print("Remaining fuel: "..statistics.lastRun.resultingFuel)
		print("Predicted fuel: "..statistics.lastRun.predictedFuel)
		print("Fuel used: "..statistics.lastRun.startingFuel - statistics.lastRun.resultingFuel)
		print("\nTotal moves: "..statistics.lastRun.totalMoves)
		print("Seeds planted: "..statistics.lastRun.seedsPlanted)
		print("Plants farmed: "..statistics.lastRun.plantsFarmed)
		print("Fertilizer used: "..statistics.lastRun.fertilizerUsed)
		sleep(1)
		pause()
	end,
	exit = function() return error(nil,0) end,
	quit = function() return error(nil,0) end,
	help = function()
		term.setCursorPos(1,1) term.clear()
		print("Help:")
		print("\nCommands: 'farm', 'help', 'exit', 'quit'")
		
		pause()
	end,
}
config.load()
checkArgs()
start()
