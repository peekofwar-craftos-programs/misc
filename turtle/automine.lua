--[[

    Program by Peekofwar

    Version 1.1.3
    June 25th, 2022

]]
toMine = {
    "minecraft:cobblestone",
    "minecraft:stone",
}
term.setCursorPos(1,1)
term.clear()
print("Cobblestone Autominer")

local function getSpace()
    local slot = turtle.getSelectedSlot()
    if turtle.getItemSpace()>0 then
        return turtle.getItemSpace()
    end
    printError("Slot full. Checking for free slot...")
    local start = turtle.getSelectedSlot()
    for a=1, 2 do
        for i=start,4*4 do
            turtle.select(i)
            if turtle.getItemSpace()>0 then
                return turtle.getItemSpace()
            end
        end
        start = 1
    end
    return 0, true
end
while true do
    term.setCursorPos(1,1) term.clear()
    print("Cobblestone Autominer")
    local space, full = getSpace()
    local present, block = turtle.inspect()
    term.setCursorPos(1,3)
    print("Space: "..space)
    if present and space > 0 then
        print("Found: "..block.name)
        for i=1, #toMine do
            if block.name == toMine[i] then
                printError("\nMining...")
                turtle.dig()
                break
            end
        end
    elseif full then
        printError("\nInventory is full!")
    elseif space <= 0 then
        printError("\nSlot overflow.")
    else
        printError("\nNothing to mine...")
    end
    if full then
        print("\nSleeping...")
        sleep(30)
    else
        print("\nCycle wait...")
        sleep(0.5)
    end
end
