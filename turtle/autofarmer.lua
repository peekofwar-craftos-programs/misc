--[[
	Indev
	(c) 2022-2024 Peekofwar
]]
local info = {
	program_name = "Autofarmer",
	version_string = "2.0.2.04",
	build_type = "release",
	build_date = "April 20, 2024",
}
print(info.program_name.." v"..info.version_string..string.sub(info.build_type,1,1).." ("..info.build_date..")")
local config_default = {
	crops = {
		["minecraft:wheat"] = {
			farmable = true,
			plantable = false,
			ripe_age = 7,
		},
		["minecraft:wheat_seeds"] = {
			plantable = true,
		},
	},
	farm = {
		plot_position = {0,0,0},
		plot_dimensions = {0,0},
		route_height = 1,
		auto_timer = 51,
	},
	home = {
		tools = {
			global_set = false,
			global_offset = {0,0,0,0},
			block_rear = "minecraft:iron_block",
			block_underside = "minecraft:barrel",
		},
	},
	fuel = {
		["minecraft:coal"] = { burnable = true, },
		["minecraft:charcoal"] = { burnable = true, },
		["minecraft:coal_block"] = { burnable = true, },
	},
	fertilizer = {
		["minecraft:bonemeal"] = true,
	},
}
local config = {}
local function config_manage(...)
	local function deepCopy(tab)
		local a = {}
		for k, v in pairs(tab) do
			if type(v) == "table" then
				a[k]=deepCopy(v)
			else
				a[k]=v
			end
		end
		return a
	end
	local function cfgverify(verify,reference)
		if type(verify) ~= "table" then return error("argument #1: expected table, got '"..type(verify).."' instead!") end
		if type(reference) ~= "table" then return error("argument #2: expected table, got '"..type(reference).."' instead!") end
		local return_boolean = false
		for key,value in pairs(reference) do
			if verify[key] == nil then
				verify[key] = reference[key]
				return_boolean = true
			elseif type(verify[key]) == "table" then
				return_boolean = cfgverify(verify[key],reference[key]) or return_boolean
			end
		end
		return return_boolean
	end

	local arguments = {...}
	local config_file = "/.program_data/autofarmer/config.lua"
	if arguments[1] == "load-all" then
		if fs.exists(config_file) then
			local file = fs.open(config_file,"r")
			config = textutils.unserialise(file.readAll())
			file.close()

			if cfgverify(config,config_default) then
				config_manage("save-all") print("Config file updated!") sleep(1.5)
			end
		else
			config = deepCopy(config_default)
		end
	elseif arguments[1] == "save-all" then
		local file = fs.open(config_file,"w")
		file.write(textutils.serialise(config))
		file.close()
	end
end

if multishell then
	local process_id = multishell.getCurrent()
	multishell.setTitle(process_id,info.program_name)
end

local wrapper = require("cc.strings").wrap
function winPrint(win,...)
	local tx,ty = term.getCursorPos()
	local w,h = win.getSize()
	local input = {...}
	local str = ""
	for i=1,#input do
		str = str..tostring(input[i])
		if i<#input then str = str.." " end
	end
	local text = wrapper(str,w)
	for i=1,#text do
		local x,y = win.getCursorPos()
		win.write(text[i])
		if y==h then win.scroll(1) win.setCursorPos(1,y) else win.setCursorPos(1,y+1) end
	end
	term.setCursorPos(tx,ty)
end

local windows = {}
local function colorPrint(color,...)
	local window = windows.main
	if type(color) == "table" then
		color = color[2]
		window = color[1]
	end
	local current = window.getTextColor()
	window.setTextColor(color)
	winPrint(window,...)
	window.setTextColor(current)
	return
end

local function splitter(inputstr, sep)
	local t={}
	for str in string.gmatch(inputstr, "([^"..(sep or " ").."]+)") do
		table.insert(t, str)
	end
	return t
end
local routines = {}
local mem = {
	suppress=false,
	cmd={
		history={},
	},
	homed = false,
	pos = {0,0,0},
	rot = 0,
	stat = {
		fuel_burnt = 0,
		fuel_estimate = 0,
		crops_harvested = 0,
		crops_planted = 0,
	},
	mode = "Manual",
	mode_state = "Stop",
	auto = {
		active = false,
		timer = 0,
	},
}
local update_header
local commands = {
	["help"] = function(...)
		local arguments = {...}
		if tonumber(arguments[1]) == 2 then
			colorPrint(colors.lightBlue," ## Help - Page 2 of 2")
			colorPrint(colors.lightGray,"> exit	   - Quits the program")
			colorPrint(colors.lightGray,"> config  - Manage config")
		else
			colorPrint(colors.lightBlue," ## Help - Page 1 of 2")
			colorPrint(colors.lightGray,"> farm	   - Run farm routine")
			colorPrint(colors.lightGray,"> stop	   - Stop current routine")
			colorPrint(colors.lightGray,"> jog	   - Move the robot")
			colorPrint(colors.lightGray,"> home	   - Homes the robot")
			colorPrint(colors.lightGray,"> pos	   - Check robot position")
			colorPrint(colors.lightGray,"> fuel [amount] - Refuel robot")
			colorPrint(colors.lightGray,"> state   - Check robot state")
			colorPrint(colors.lightGray,"> stats   - See last run stats")
			colorPrint(colors.lightGray,"> gui	   - Opens graphical interface")
			colorPrint(colors.lightGray,"> version - Prints version")
		end
	end,
	["version"] = function()
		colorPrint(colors.yellow,"v"..info.version_string.." "..info.build_type.." ("..info.build_date..")")
	end,
	["gui"] = function()
		--return colorPrint(colors.red,"Sorry, but there's no GUI interface available for this program.")
		windows.main.setVisible(false)
		windows.commandLine.setVisible(false)
		windows.gui.clear()
		windows.gui.setVisible(true)
		windows.gui.setCursorPos(2,2)
		windows.gui.write("Press DEL to exit")
		while true do
			local event, key = os.pullEvent("key")
			if key == keys.delete then
				break
			end
		end
		windows.main.setVisible(true)
		windows.commandLine.setVisible(true)
		windows.gui.setVisible(false)
	end,
	["config"] = function(...)
		local arguments = {...}
		if arguments[1] == "save-all" then
			config_manage("save-all")
			colorPrint(colors.green,"Saving config...")
		elseif arguments[1] == "help" then
			colorPrint(colors.lightGray,"> save-all   - Saves the config to disk")
		else
			colorPrint(colors.red,"Unknown function. Run 'config help' for help.")
		end
	end,
	["clear"] = function()
		windows.main.setCursorPos(1,1)
		windows.main.clear()
	end,
	["echo"] = function(...)
		colorPrint(colors.white,...)
	end,
	["exit"] = function()
		colorPrint(colors.red,"Closing program...")
	end,
	["suppress"] = function(state)
		if state == nil then
			return colorPrint(colors.lightGray,"Suppress program output: "..mem.suppress and "Suppressed" or "Not suppressed")
		elseif string.lower(state) == "on" then
			mem.suppress = true
			return colorPrint(colors.yellow,"Suppression enabled")
		elseif string.lower(state) == "off" then
			mem.suppress = false
			return colorPrint(colors.green,"Suppression disabled")
		else
			return colorPrint(colors.red,"Usage: suppress [on/off]\nProvide no arguments to check state.")
		end
	end,
	["position"] = function()
		colorPrint(not mem.homed and colors.red or colors.cyan,mem.homed and ("Local pos: ("..mem.pos[1]..","..mem.pos[2]..","..mem.pos[3].."), facing "..(mem.rot == 0 and "North" or mem.rot == 1 and "East" or mem.rot == 2 and "South" or mem.rot == 3 and "West" or "UNKNOWN")) or "Not homed.")
	end,
	["home"] = function()
		local homed = routines.home()
		colorPrint(homed and colors.green or colors.red,homed and "Robot homed." or "Robot is not at base station!")
		return
	end,
	["jog"] = function()
		colorPrint(colors.lightGray,"Use WASD to move, LSHIFT and LCTRL to move vertically.\nPress BACKSPACE to exit.")
		colorPrint(mem.homed and colors.green or colors.red,mem.homed and "Robot homed." or "Robot is not homed.")
		colorPrint(turtle.getFuelLevel() >= 10 and colors.yellow or colors.red,turtle.getFuelLevel() > 0 and tostring(turtle.getFuelLevel()) or "NO FUEL! Robot cannot move.")
		mem.stat.fuel_burnt = 0
		while true do
			local event, key = os.pullEvent("key")
			if key == keys.w then routines.move("forward")
			elseif key == keys.s then routines.move("back")
			elseif key == keys.leftShift then routines.move("up")
			elseif key == keys.leftCtrl then routines.move("down")
			elseif key == keys.a then routines.move("left","rotate")
			elseif key == keys.d then routines.move("right","rotate")
			elseif key == keys.backspace then 
				colorPrint(colors.yellow,"Exiting jog mode...") 
				colorPrint(colors.lightGray,"Fuel burnt: "..mem.stat.fuel_burnt) 
				return
			end
			if key == keys.w or key == keys.s or key == keys.leftShift or key == keys.leftCtrl then
				if mem.homed then
					colorPrint((not mem.homed or turtle.getFuelLevel() == 0) and colors.red or colors.cyan,mem.homed and ("Local pos: ("..mem.pos[1]..","..mem.pos[2]..","..mem.pos[3]..")")..(turtle.getFuelLevel() == 0 and " NO FUEL" or "") or "Not homed.")
				end
			elseif (key == keys.a or key == keys.d) then
				if mem.homed then 
					colorPrint(not mem.homed and colors.red or colors.cyan,mem.homed and "Facing "..(mem.rot == 0 and "North" or mem.rot == 1 and "East" or mem.rot == 2 and "South" or mem.rot == 3 and "West" or "UNKNOWN")) 
				end
			end
		end
	end,
	["fuel"] = function(...)
		local arguments = {...}
		if #arguments > 0 then
			arguments[1] = tonumber(arguments[1])
			if type(arguments[1]) == "number" then
				local ok, result = routines.refuel(arguments[1])
				colorPrint(ok and colors.green or colors.red,ok and "Successfully refueled." or "Couldn't find '"..result.."' items to burn.")
			else
				colorPrint(colors.red,"Fuel to be consumed must be a number.")
			end
		else
			local fl = turtle.getFuelLevel()
			colorPrint(fl>10 and colors.cyan or colors.red,fl>0 and "Fuel: "..fl or "NO FUEL!")
		end
	end,
	["farm"] = function(...)
		local arguments = {...}
		if arguments[1] == "auto" then
			mem.auto.active = true
			mem.mode = "Farm"
			if arguments[2] == "wait" then
				mem.mode = "Sleep"
				mem.mode_state = "Sleep"
				mem.auto.timer = config.farm.auto_timer*60 or 51*60
				colorPrint(colors.yellow,"Timer set for "..tostring(config.farm.auto_timer).." minutes.")
				os.queueEvent("robot_set_timer")
			else
				colorPrint(colors.green,"Farming mode set.")
				os.queueEvent("robot_wake")
			end
		elseif arguments[1] == "help" then
			colorPrint(colors.lightGray,"> auto [wait]  - Enable timer cycle")
			colorPrint(colors.lightGray,"Adding 'wait' will only trigger the timer instead of sending the robot out immediately.")
		else
			mem.mode = "Farm"
			colorPrint(colors.green,"Farming mode set.")
			os.queueEvent("robot_wake")
		end
	end,
	["stop"] = function()
		colorPrint(colors.yellow,"Stopping current routine...")
		mem.mode = "Manual"
		if mem.mode_state == "Run" then 
			mem.homed = false
			colorPrint(colors.red,"Warning: Robot no longer homed.")
		end
		mem.mode_state = "Stop"
		mem.auto.active = false
		mem.auto.timer = 0
		update_header()
	end,
	["state"] = function()
		colorPrint(colors.lightGray,"Mode: "..mem.mode)
		colorPrint(colors.lightGray,"State: "..mem.mode_state)
		colorPrint(colors.lightGray,"Auto active: "..(mem.auto.active and "yes" or "no"))
		colorPrint(colors.lightGray,"Auto timer: "..tostring(mem.auto.timer))
	end,
}
commands["quit"] = function() return commands["exit"]() end
commands["cls"] = function() return commands["clear"]() end
commands["pos"] = function() return commands["position"]() end

function routines.clearStats()
	mem.stat = {
		fuel_burnt = 0,
		fuel_estimate = 0,
		crops_harvested = 0,
		crops_planted = 0,
	}
end
function routines.home()
	local block_present, block_data = turtle.inspectDown()
	if block_present and block_data.name == config.home.tools.block_underside then
		for i=1, 4 do
			local block, data = turtle.inspect()
			if block and data.name == config.home.tools.block_rear then
				turtle.turnLeft()
				turtle.turnLeft()
				mem.pos = {0,0,0}
				mem.rot = 0
				mem.homed = true
				return true
			else
				turtle.turnLeft()
			end
		end
	else
		return false
	end
end
function routines.move(direction,mode)
	local ok = false
	local function translate(dir)
		ok = true
		if dir == "up" then
			mem.pos[3] = mem.pos[3] + 1
		elseif dir == "down" then
			mem.pos[3] = mem.pos[3] - 1
		elseif mem.rot == 0 then
			mem.pos[2] = mem.pos[2] + (dir == "forward" and 1 or -1)
		elseif mem.rot == 1 then
			mem.pos[1] = mem.pos[1] + (dir == "forward" and 1 or -1)
		elseif mem.rot == 2 then
			mem.pos[2] = mem.pos[2] - (dir == "forward" and 1 or -1)
		elseif mem.rot == 3 then
			mem.pos[1] = mem.pos[1] - (dir == "forward" and 1 or -1)
		end
		mem.stat.fuel_burnt = mem.stat.fuel_burnt + 1
	end
	if mode == "rotate" then
		ok = true
		if direction == "left" then
			turtle.turnLeft()
			mem.rot = mem.rot - 1
		elseif direction == "right" then
			turtle.turnRight()
			mem.rot = mem.rot + 1
		end
	elseif mode == "face" then
		ok = true
		local data = {["North"]=0,["East"]=1,["South"]=2,["West"]=3,}
		if not data[direction] then return error("'"..direction.."' is not a valid direction.") end
		local rot = data[direction]
		if	   mem.rot == 0 and rot == 1 then routines.move("right","rotate")
		elseif mem.rot == 0 and rot == 2 then routines.move("left","rotate") routines.move("left","rotate")
		elseif mem.rot == 0 and rot == 3 then routines.move("left","rotate")
		elseif mem.rot == 1 and rot == 2 then routines.move("right","rotate")
		elseif mem.rot == 1 and rot == 3 then routines.move("left","rotate") routines.move("left","rotate")
		elseif mem.rot == 1 and rot == 0 then routines.move("left","rotate")
		elseif mem.rot == 2 and rot == 3 then routines.move("right","rotate")
		elseif mem.rot == 2 and rot == 0 then routines.move("left","rotate") routines.move("left","rotate")
		elseif mem.rot == 2 and rot == 1 then routines.move("left","rotate")
		elseif mem.rot == 3 and rot == 0 then routines.move("right","rotate")
		elseif mem.rot == 3 and rot == 1 then routines.move("left","rotate") routines.move("left","rotate")
		elseif mem.rot == 3 and rot == 2 then routines.move("left","rotate")
		end
	else
		if turtle.getFuelLevel() < 1 then return false, "fuel" end
		if direction == "forward" then
			if turtle.forward() then translate("forward") end
		elseif direction == "back" then
			if turtle.back() then translate("backward") end
		elseif direction == "up" then
			if turtle.up() then translate("up") end
		elseif direction == "down" then
			if turtle.down() then translate("down") end
		end
	end
	if mem.rot < 0 then mem.rot = mem.rot + 4 elseif mem.rot > 3 then mem.rot = mem.rot - 4 end
	return ok
end
function routines.refuel(amount)
	amount = amount or 1
	local function fuel()
		local item = turtle.getItemDetail() and turtle.getItemDetail().name
		local count = turtle.getItemCount()
		if config.fuel[item] and config.fuel[item].burnable then
			turtle.refuel(count >= amount and amount or count)
			if count >= amount and amount then 
				amount = amount - amount
			else
				amount = amount - count
			end
		end
	end
	fuel()
	if amount == 0 then return true end
	for i=1, 4*4 do
		if amount == 0 then return true end
		turtle.select(i)
		fuel()
	end
	if amount == 0 then return true end
	return false, amount
end
function routines.checkFuel() -- Slightly innaccurate, needs improvement
	local requirement = 0
	requirement = requirement + 5 -- Compensate for estimation errors
	
	requirement = requirement + config.farm.route_height -- Move up from home
	requirement = requirement + config.farm.plot_position[2] -- Move to plot Y
	requirement = requirement + config.farm.route_height - math.abs(config.farm.plot_position[3]+1) -- Move down to plot Z
	requirement = requirement + math.abs(config.farm.plot_position[1]) -- Move to plot X
	requirement = requirement + config.farm.plot_dimensions[1]*config.farm.plot_dimensions[2] -- Traverse entire plot
	local isEven = config.farm.plot_dimensions[2]/2 == math.floor(config.farm.plot_dimensions[2]/2)
	if isEven then
		-- Ends on right side
		requirement = requirement + config.farm.plot_dimensions[1] -- Move left to x=0
	else
		-- Ends on left side
		requirement = requirement + config.farm.plot_position[1] -- Move left to x=0
	end
	requirement = requirement + config.farm.plot_dimensions[2] - config.farm.plot_position[2] -- Move back to plot edge
	requirement = requirement + config.farm.route_height - config.farm.plot_position[3] -- Move up from plot
	requirement = requirement + config.farm.plot_position[2] -- Move back to y=0
	requirement = requirement + config.farm.route_height -- Return to base station
	
	--[[ -- old fuel check code
	local total_route_height = config.farm.route_height + math.abs(config.farm.plot_position[3])
	requirement = requirement + total_route_height * 2 -- route height
	requirement = requirement + math.abs(config.farm.plot_position[2]) -- Move to y offset
	requirement = requirement + math.abs(config.farm.plot_position[1]) -- Move to x offset
	requirement = requirement + math.abs(total_route_height - config.farm.plot_position[3]) -- Move to z offset
	requirement = requirement + config.farm.plot_dimensions[1]*config.farm.plot_dimensions[2] -- Traverse entire plot
	requirement = requirement + math.abs(config.farm.plot_position[2]+config.farm.plot_dimensions[2]) -- Return to y 0
	requirement = requirement + math.abs(config.farm.plot_position[1]+config.farm.plot_dimensions[1]) -- Return to x 0
	]]
	
	local fuel = turtle.getFuelLevel()
	if fuel < requirement then return false, requirement
	else return true, requirement end
end
function routines.farm_subroutine()
	local function isRipe()
		local present, data = turtle.inspectDown()
		if data and config.crops[data.name] and data.state and data.state.age and data.state.age == config.crops[data.name].ripe_age then
			return true
		else
			return false
		end
	end
	local function plantCrop()
		if turtle.inspectDown() then return end
		local item = turtle.getItemDetail()
		if item and config.crops[item.name] and config.crops[item.name].plantable then
			if turtle.placeDown() then mem.stat.crops_planted = mem.stat.crops_planted + 1 end
			return
		end
		for i=1, 4*4 do
			turtle.select(i)
			local item = turtle.getItemDetail()
			if item and config.crops[item.name] and config.crops[item.name].plantable then
				if turtle.placeDown() then mem.stat.crops_planted = mem.stat.crops_planted + 1 end
				return
			end
		end
	end
	local function fertilizer()
		if isRipe() then return end
		-- add code here
	end
	fertilizer()
	if isRipe() then
		if turtle.digDown() then mem.stat.crops_harvested = mem.stat.crops_harvested + 1 end
	end
	plantCrop()
end
function routines.flushInventory()
	for i=1, 4*4 do
		turtle.select(i)
		local item = turtle.getItemDetail() and turtle.getItemDetail().name
		if not config.fuel[item] then
			turtle.dropDown()
		end
	end
end
function routines.farm()
	routines.clearStats()
	mem.mode_state = "Star"
	local function hasCrashed()
		mem.mode_state = "Error"
		colorPrint(colors.red,"Robot has crashed!")
		return
	end
	if not mem.homed then 
		colorPrint(colors.yellow,"Checking for base station...") 
		if not routines.home() then
			colorPrint(colors.red,"Robot position is unknown.")
			mem.mode_state = "Error"
			return false
		end
	end
	if mem.pos[1] ~= 0 or mem.pos[2] ~= 0 or mem.pos[3] ~= 0 then
		mem.mode_state = "Error"
		colorPrint(colors.red,"Robot must start at base station.")
		return false
	end
	colorPrint(colors.lightGray,"Checking routine fuel requirements...")
	local _, result = routines.checkFuel()
	mem.stat.fuel_estimate = result
	if turtle.getFuelLevel() < result then
		local ok
		while true do
			if turtle.getFuelLevel() >= result then break end
			local ok = routines.refuel()
			if not ok then break end
		end
		if turtle.getFuelLevel() < result then
			colorPrint(colors.red,"Insuficient fuel; cannot proceed.")
			colorPrint(colors.red,"Fuel: "..turtle.getFuelLevel().."/"..result)
			mem.mode_state = "Error"
			return false
		end
	end
	colorPrint(colors.green,"All checks have passed.")
	
	mem.mode_state = "Run"
	
	local move = routines.move
	local farm = config.farm
	
	move("North","face")
	while mem.pos[3] < farm.route_height do -- Move up from home
		if not move("up") then hasCrashed() return end
	end
	colorPrint(colors.lightGray,"Moving to y "..farm.plot_position[2].."...")
	while mem.pos[2] < farm.plot_position[2] do
		if not move("forward") then hasCrashed() return end
	end
	-- return to plot-level for farming
	while mem.pos[3] > farm.plot_position[3]+1 do
		if not move("down") then hasCrashed() return end
	end
	while mem.pos[3] < farm.plot_position[3]+1 do
		if not move("up") then hasCrashed() return end
	end
	
	colorPrint(colors.lightGray,"Moving to x "..farm.plot_position[1].."...")
	if mem.pos[1] < farm.plot_position[1] then
		move("East","face")
		while mem.pos[1] < farm.plot_position[1] do
			if not move("forward") then hasCrashed() return end
		end
	elseif mem.pos[1] > farm.plot_position[1] then
		move("West","face")
		while mem.pos[1] > farm.plot_position[1] do
			if not move("forward") then hasCrashed() return end
		end
	end
	colorPrint(colors.cyan,"Arrived at plot corner.")
	local isWest = false
	for i=1, farm.plot_dimensions[2] do
		for i=1, farm.plot_dimensions[1] do
			--x axis
			routines.farm_subroutine()
			if i < farm.plot_dimensions[1] then
				move(isWest and "West" or "East","face")
				if not move("forward") then hasCrashed() return end
			end
		end
		--y axis
		if i < farm.plot_dimensions[2] then
			move("North","face")
			if not move("forward") then hasCrashed() return end
			isWest = not isWest
		end
	end
	-- RETURN PATH
	colorPrint(colors.lightGray,"Returning to x 0...")
	if mem.pos[1] < 0 then
		move("East","face")
		while mem.pos[1] < 0 do
			if not move("forward") then hasCrashed() return end
		end
	elseif mem.pos[1] > 0 then
		move("West","face")
		while mem.pos[1] > 0 do
			if not move("forward") then hasCrashed() return end
		end
	end
	colorPrint(colors.lightGray,"Exitting plot...")
	move("South","face")
	while mem.pos[2] > farm.plot_position[2] do
		if not move("forward") then hasCrashed() return end
	end
	while mem.pos[3] < farm.route_height do -- Move up
		if not move("up") then hasCrashed() return end
	end
	colorPrint(colors.lightGray,"Returning to base station...")
	while mem.pos[2] > 0 do
		if not move("forward") then hasCrashed() return end
	end
	while mem.pos[3] > 0 do
		if not move("down") then hasCrashed() return end
	end
	move("North","face")
	
	colorPrint(colors.lightGray,"Burned "..mem.stat.fuel_burnt.."/"..mem.stat.fuel_estimate.." of estimated fuel.")
	
	routines.flushInventory()
	
	mem.mode_state = "End"
	return
end
local function thread_autofarm_timer()
	while true do
		if mem.auto.timer == 0 then
			local event = {os.pullEvent("robot_set_timer")}
		end
		while mem.auto.timer > 0 do
			mem.auto.timer = mem.auto.timer - 1
			update_header()
			sleep(1)
		end
		if mem.auto.active and mem.auto.timer == 0 then
			mem.mode = "Farm"
			os.queueEvent("robot_wake")
		end
	end
end

local function commandLine()
	local current = term.current()
	term.redirect(windows.commandLine)
	term.setCursorPos(1,1)
	term.clear()
	term.setTextColor(colors.white)
	write("> ")
	local command = read(nil,mem.cmd.history)
	term.redirect(current)
	return #command > 0 and command or nil
end

local function command_entry()
	colorPrint(colors.white,"Enter 'help' for help.")
	while true do
		local command = commandLine()
		if command and #command > 0 then
			table.insert(mem.cmd.history, command)
			colorPrint(colors.yellow,"> "..command)
			local args = splitter(command)
			local cmd = args[1]
			table.remove(args,1)
			if cmd and commands[string.lower(cmd)] then
				commands[string.lower(cmd)](table.unpack(args))
			end
			if cmd and (string.lower(cmd) == "exit" or string.lower(cmd) == "quit") then
				break
			end
		end
	end
end

local pretty = require("cc.pretty")
local function main()
	local colorPrint = colorPrint
	do -- Suppression override
		local cp = colorPrint
		function colorPrint(...)
			if not mem.suppress then
				return cp(...)
			end
		end
	end
	local function getEvent()
		while true do
			local event = {os.pullEvent()}
			if event[1] == "modem_message" or event[1] == "peripheral" or event[1] == "peripheral_detach" or event[1] == "robot_wake" then
				return event
			end
		end
	end
	local modem
	colorPrint(colors.green,"\nAutofarmer ready...")
	while true do
		local event = getEvent()
		--pretty.print(pretty.pretty(event))
		if event[1] == "robot_wake" then
			update_header()
			if mem.mode == "Farm" then
				mem.auto.timer = 0
				update_header()
				parallel.waitForAny(
					routines.farm,
					function()
						while true do
							if mem.mode == "Manual" or mem.mode_state == "Stop" or mem.mode_state == "End" or mem.mode_state == "Error" then
								return
							end
							sleep(1)
						end
					end
				) colorPrint(colors.yellow,"Farming routine stopped.")
			end
		end
		if mem.mode == "Farm" and mem.mode_state == "End" and mem.auto.active then
			mem.mode = "Sleep"
			mem.mode_state = "Sleep"
			mem.auto.timer = config.farm.auto_timer*60 or 51*60
			colorPrint(colors.yellow,"Timer set for "..tostring(config.farm.auto_timer).." minutes.")
			os.queueEvent("robot_set_timer")
		end
	end
end

local function crashScreen(...)
	local pass, err = ...

	local function cWrite(text)
		local w, h = term.getSize()
		local cX,cY = term.getCursorPos()
		term.setCursorPos(math.floor(w / 2 - text:len() / 2 + .5), cY)
		io.write(text)
	end

	if not pass and err then
		if err == nil then err = 'UNKNOWN ERROR\n\nAn error was thrown with no message provided.' end
		
		term.setBackgroundColor(colors.black)
		term.setTextColor(colors.white)
		term.clear()
		
		term.setBackgroundColor(colors.red)
		paintutils.drawFilledBox(1,0,51,3)
		term.setCursorPos(1,2)
		cWrite('Fatal Error!')
		
		term.setBackgroundColor(colors.black)
		printError('\n\n\n'..err..'\n')
	end
end
local w,h = term.getSize()
function update_header()
	local x,y = term.getCursorPos()
	local c = term.getTextColor()
	local function cWrite(text)
		local w, h = windows.header.getSize()
		local cX,cY = windows.header.getCursorPos()
		windows.header.setCursorPos(math.floor(w / 2 - text:len() / 2 + .5), cY)
		windows.header.write(text)
	end
	windows.header.setCursorPos(1,1)
	windows.header.clear()
	local version = splitter(info.version_string,".")
	windows.header.write(info.program_name)
	local verstr = version[1].."."..version[2].."."..version[3]..string.sub(info.build_type,1,1)..version[4]
	windows.header.setCursorPos(w-#verstr,1)
	windows.header.write(verstr)

	cWrite(mem.auto.timer > 0 and textutils.formatTime(mem.auto.timer/60,true) or "")
	term.setCursorPos(x,y)
	term.setTextColor(c)
end
local function startup()
	local currentTerm = term.current()
	local pass,err = pcall(function()
		local w,h = term.getSize()
		term.setCursorPos(1,1)
		term.clear()

		print("Loading config...")
		config_manage("load-all")

		if err then error(err) end
		print("finished")

		windows.header = window.create(term.current(),1,1,w,1)
		windows.header.setBackgroundColor(colors.lightGray)
		windows.header.clear()

		windows.main = window.create(term.current(),1,2,w,h-1)
		windows.main.setBackgroundColor(colors.black)
		windows.main.clear()

		windows.gui = window.create(term.current(),1,2,w,h,false)
		windows.gui.setBackgroundColor(colors.cyan)
		windows.gui.clear()

		windows.commandLine = window.create(term.current(),1,h,w,1)
		windows.commandLine.setBackgroundColor(colors.black)
		windows.commandLine.clear()

		windows.header.setTextColor(colors.black)
		
		update_header(w,h)

		parallel.waitForAny(command_entry,main,thread_autofarm_timer)
	end)
	term.redirect(currentTerm)
	if not pass then sleep(1) else term.clear() term.setCursorPos(1,1) end
	crashScreen(pass,err)
end

--local pullEvent = os.pullEvent
--os.pullEvent = os.pullEventRaw
startup()
--os.pullEvent = pullEvent
