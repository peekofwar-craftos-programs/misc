--[[

    Automatic Obsidian Generator
    Program by Peekofwar

    Version 1.0.0
    July 9th, 2022

]]

local config = {
    output_container = "right",
    input_container = "bottom",
}

local function checkInventory()
    local item = turtle.getItemDetail()
    if item and item.name == "minecraft:bucket" then
        printError("Ejecting empty bucket...")
        turtle.dropDown()
    end
end

local periph = {}

local function main()
    periph.output = peripheral.wrap(config.output_container)
    
    print("Automatic Obsidian Production\n")
    printError("Calibrating...")
    while true do
        redstone.setOutput("top",true) sleep(0)
        redstone.setOutput("top",false)
        sleep(1)
        local present, blockdata
        for i=1, 4 do
            present, blockdata = turtle.inspect()
            if blockdata and (blockdata.name == "minecraft:water" or blockdata.name == "minecraft:obsidian") then
                break
            else
                turtle.turnLeft()
                sleep(0.5)
            end
        end
        if blockdata and (blockdata.name == "minecraft:water" or blockdata.name == "minecraft:obsidian") then
            print("Calibrated.")
            redstone.setOutput("top",true) sleep(0)
            redstone.setOutput("top",false)
            sleep(1)
            break
        end
    end
    while true do
        term.setCursorPos(1,1) term.clear()
        print("Automatic Obsidian Production\n")
        
        checkInventory()

        local present, blockdata = turtle.inspect()
        if present and blockdata.name == "minecraft:obsidian" then
            print("Mining Obsidian...")
            turtle.dig()
        elseif present and blockdata.name == "minecraft:water" then
            printError("Fixing incorrect dispenser state...")
            redstone.setOutput("top", true)
            sleep(0)
            redstone.setOutput("top",false)
        elseif present and blockdata.name == "minecraft:lava" then
            print("Dispensing water...")
            for i=1, 2 do
                redstone.setOutput("top", true)
                sleep(0)
                redstone.setOutput("top",false)
                sleep(0.125)
            end
        elseif not present then
            local item = turtle.getItemDetail()
            if item and item.name == "minecraft:obsidian" then
                print("Offloading obsidian to container...")
                turtle.turnRight()
                local sucsess = turtle.drop()
                turtle.turnLeft()
                if not sucsess then
                    printError("Container is full!")
                    sleep(60)
                end
            elseif item and item.name == "minecraft:lava_bucket" then
                printError("Ejecting bucket...")
                turtle.dropDown()
            else
                print("Fetching lava bucket...")
                if turtle.suckDown() then
                    print("Dispensing lava...")
                    turtle.place()
                    turtle.dropDown()
                else
                     printError("Waiting for lava bucket...")
                     sleep(10)
                end
            end
        end
        sleep(0.5)
    end
end

main()
